require 'rails_helper'

RSpec.describe User, type: :model do

  context 'when create user with defaul role' do
    before do
      @user = User.create(
        email: "user@ua.fm",
        password: "password"
      )
    end
    it 'set defaul role' do
      expect(@user.set_default_role).to eq "user"
    end

  end
end
