require 'rails_helper'

RSpec.describe Movie, type: :model do
  let!(:movie) { create(:movie) }
  context 'when failing validations' do
    it 'is not valid without title' do
      movie.title = nil
      expect(movie).to_not be_valid
    end
    it 'is not valid without description' do
      movie.description = nil
      expect(movie).to_not be_valid
    end
  end

  context 'should have preview_img' do
    it { is_expected.to validate_attached_of(:preview_img)}
  end
end
