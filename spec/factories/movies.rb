FactoryBot.define do
  factory :movie do
    title { 'Spider Man' }
    description { 'With Spider-Mans identity now revealed, Peter asks Doctor Strange for help. When a spell goes wrong, dangerous foes from other worlds start to appear, forcing Peter to discover what it truly means to be Spider-Man.' }
    preview_img { Rack::Test::UploadedFile.new('app/assets/images/image1.jpg') }
    category { Category.create(name: "Бойовик") }
    user { User.create(email: "example@ua.fm", password: "password", role: 1)}
  end
end
