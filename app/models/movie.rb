class Movie < ApplicationRecord
  
  has_rich_text    :description
  has_one_attached :preview_img
  belongs_to       :user
  belongs_to       :category
  has_many         :ratings, dependent: :destroy




  validates :title, presence: true, length: { maximum: 80 }
  validates :description, presence: true, length: { minimum: 20 }
  validates :preview_img, attached: true, content_type: %i[png jpg jpeg]
  validates :category_id, presence: true

  def average_rating
    ratings.average(:star).to_f.round(1)
  end

  def image_resize
    preview_img.variant(resize_to_limit: [800, 600]).processed
  end
end
