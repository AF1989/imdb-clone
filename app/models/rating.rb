class Rating < ApplicationRecord
  MAX_RATING = 10
  belongs_to :movie
  belongs_to :user
  accepts_nested_attributes_for :movie


  validates :star, numericality: {in: 0..MAX_RATING}

  
end
