class RatingsController < ApplicationController
  before_action :authenticate_user!

  def index
    @ratings = Rating.all
  end  

  def create
    @rating = Rating.create!(rating_params.merge(user_id: current_user.id))
    redirect_to root_path
  end


  private

  def rating_params
    params.require(:rating).permit(:star, :movie_id)
  end

end