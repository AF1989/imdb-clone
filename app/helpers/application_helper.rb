module ApplicationHelper
  def movie_categories
    @categories = Category.all.order(:name)
  end
end
