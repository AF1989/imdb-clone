module RatingHelper
  def star_rating_class(movie, index)
    if index < movie.average_rating
      "fill: yellow; stroke: yellow"
    else
      "fill: transparent; stroke: gray"
    end
  end

      
end