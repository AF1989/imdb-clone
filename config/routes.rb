Rails.application.routes.draw do
  resources :movies do
    resources :ratings, only: [:create, :destroy]
  end
  resources :movies
  resources :categories
  devise_for :users
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "movies#index"
  root "movies#index"
end
