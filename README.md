# README

Getting started
To get started with the app, clone the repo and then install the needed gems:
$ bundle install

Next, migrate the database:
$ rails db:migrate

Next, rub seed
$rails db:seed

Finally, run the test suite to verify that everything is working correctly:
$ rspec spec

I hope rspec tests suite will be passes. You'll be ready to run the app in a local server, but before that cross your fingers for luck:
$ rails server